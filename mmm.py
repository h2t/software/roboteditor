import bpy
from mathutils import Euler, Vector
from math import pi
import xml.etree.ElementTree as etree
import itertools
from . import armatures

def tolower(element):
    """Convert all tags in the XML file to lower case."""
    element.tag = element.tag.lower()
    for i in element.getchildren():
        tolower(i)

def read(motion, version):
    """Read the MMM File from disc and sets keyframes."""
    start=bpy.context.scene.frame_current
    fps=bpy.context.scene.render.fps
    scale_factor=0.001
    tolower(motion)

    if version == "2.0":
        if motion.get("synchronized") == "true":
            names=[e.get("name").replace('_joint','') for e in motion.findall("./sensors/sensor[@type='Kinematic']/configuration/joint")]
            timestamps=[float(e.get("timestep").strip()) for e in motion.findall("./sensors/sensor[@type='ModelPose']/data/measurement")]
            if (len(timestamps) > 1):
                fps = round(1.0 / (timestamps[1] - timestamps[0]))
                print("Calculated FPS: ", fps)
            root_positions=[[float(i)*scale_factor for i in e.text.strip().split()] for e in motion.findall("./sensors/sensor[@type='ModelPose']/data/measurement/rootposition")]
            root_rotations=[[float(i) for i in e.text.strip().split()] for e in motion.findall("./sensors/sensor[@type='ModelPose']/data/measurement/rootrotation")]

            joints = []
            for kinematicSensor in motion.findall("./sensors/sensor[@type='Kinematic']"):
                joints.append([[float(i) for i in e.text.strip().split()] for e in kinematicSensor.findall("./data/measurement/jointposition")])
            shape = len(timestamps)
            joint_positions = [[val for lst in joints for val in lst[i]] for i in range(shape)]
        else:
            print("Error, motion is not synchronized. Use MMMSynchronize in the MMMViewer.")
    else:
        names=[e.get("name").replace('_joint','') for e in motion.findall(".//jointorder/joint")]

        timestamps=[float(e.text.strip()) for e in motion.findall(".//motionframes/motionframe/timestep")]
        root_positions=[[float(i)*scale_factor for i in e.text.strip().split()] for e in motion.findall(".//motionframes/motionframe/rootposition")]
        root_rotations=[[float(i) for i in e.text.strip().split()] for e in motion.findall(".//motionframes/motionframe/rootrotation")]
        joint_positions=[[float(i) for i in e.text.strip().split()] for e in motion.findall(".//motionframes/motionframe/jointposition")]

    missing = []
    for i in names:
        if not i in bpy.context.object.data.bones.keys():
            print("Could not find joint: %s" % i)
            names.remove(i)
            missing.append(i)

    print(missing)

    print(len(root_positions), len(root_rotations))
    bpy.ops.object.mode_set(mode='OBJECT')

    #lastFrame = start -20
    frameCounter = start        # count the current frame in blender
    
    # disable kinematic updates for import 
    bpy.context.scene.RobotEditor.doKinematicUpdate = False

    for [i,[timestamp,root_position,root_rotation,joint_position]] in enumerate(itertools.zip_longest(timestamps,root_positions,root_rotations,joint_positions,fillvalue=[])):

        #bpy.context.scene.frame_current = start + timestamp * fps * 10
        bpy.context.scene.frame_current = frameCounter      # set current frame in blender
        frameCounter = frameCounter + 1                     # increase frameCounter for next frame
        #if bpy.context.scene.frame_current - lastFrame < 12: #or bpy.context.scene.frame_current > 100:
        #    print('Skipping')
        #    continue
        #lastFrame = bpy.context.scene.frame_current

        print("Frame number: " , bpy.context.scene.frame_current , " of " , len(root_positions)-1)
        armName = bpy.context.active_object.name
        boneName = bpy.context.active_object.data.bones[0].name
        bpy.context.active_object.location = Vector(root_position)
        bpy.context.active_object.rotation_euler = Euler(root_rotation, "XYZ")

        bpy.context.active_object.keyframe_insert('location')
        bpy.context.active_object.keyframe_insert('rotation_euler')

        for [x,value] in enumerate(joint_position):
            if x < len(names):
                bpy.ops.roboteditor.selectbone(boneName = names[x])
                try:
                    bpy.context.active_bone.RobotEditor.theta.value=value/pi*180.0
                except KeyError:
                    print("Error updating %s" %s)
                #print(names[x], value/pi*180, bpy.context.active_bone.RobotEditor.theta.value)

        bpy.ops.object.mode_set(mode='POSE')
        bpy.ops.pose.select_all(action='SELECT')
        armatures.updateKinematics(armName,boneName)
        bones = bpy.context.active_object.pose.bones
        for b in bones:
            b.keyframe_insert('rotation_quaternion', group = b.name)
        bpy.ops.object.mode_set(mode='OBJECT')

    height = motion.find(".//height")
    if height is not None:
        modelScaleFactor = float(height.text)
        bpy.context.active_object.scale.x = modelScaleFactor
        bpy.context.active_object.scale.y = modelScaleFactor
        bpy.context.active_object.scale.z = modelScaleFactor

    bpy.context.scene.RobotEditor.doKinematicUpdate = True
